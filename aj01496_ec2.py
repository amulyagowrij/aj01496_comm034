#!/usr/bin/env python3
import math, random, sys, json
from statistics import mean, stdev
import logging

# Setup logging
logging.basicConfig(filename='/var/log/ec2_script.log', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

try:
    # Read and parse the incoming JSON payload
    input_data = sys.stdin.read()
    logging.debug(f"Received input data: {input_data}")
    event = json.loads(input_data)

    close = event.get('close')
    buy = event.get('buy')
    sell = event.get('sell')
    dt = event.get('dates')
    h = int(event.get('h'))
    d = int(event.get('d'))
    t = event.get('t')

    if None in [dt, close, buy, sell, h, d, t]:
        raise ValueError("Missing one or more necessary keys in the input data.")

    minhistory = h
    shots = d
    var95_list = []
    var99_list = []
    dates = []

    for i in range(minhistory, len(close)):
        if t == "buy" and buy[i] == 1:
            close_data = close[i-minhistory:i]
            pct_change = [(close_data[j] - close_data[j-1]) / close_data[j-1] for j in range(1, len(close_data))]
            mn = mean(pct_change)
            std = stdev(pct_change)
            simulated = [random.gauss(mn, std) for _ in range(shots)]
            simulated.sort(reverse=True)
            var95_list.append(simulated[int(len(simulated) * 0.95)])
            var99_list.append(simulated[int(len(simulated) * 0.99)])
            dates.append(str(dt[i]))
        elif t == "sell" and sell[i] == 1:
            close_data = close[i-minhistory:i]
            pct_change = [(close_data[j] - close_data[j-1]) / close_data[j-1] for j in range(1, len(close_data))]
            mn = mean(pct_change)
            std = stdev(pct_change)
            simulated = [random.gauss(mn, std) for _ in range(shots)]
            simulated.sort(reverse=True)
            var95_list.append(simulated[int(len(simulated) * 0.95)])
            var99_list.append(simulated[int(len(simulated) * 0.99)])
            dates.append(str(dt[i]))

    output = {
        "dates": dates,
        "var95": var95_list,
        "var99": var99_list
    }

    logging.debug(f"Produced output: {json.dumps(output)}")

    print("Content-Type: application/json")
    print()
    print(json.dumps(output))

except Exception as e:
    error_message = str(e)
    logging.error(f"Error: {error_message}")
    print("Content-Type: text/plain")
    print()
    print(error_message)
